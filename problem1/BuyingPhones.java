import java.util.Scanner;

public class BuyingPhones {

	public static void main(String[] args) {
			
			System.out.println("Enter the regular price of a phone: ");
			Scanner s = new Scanner(System.in);
			double price = s.nextDouble();
			
			double phone1 = price;
			double phone2 = price;
			double phone3 = price / 2;
			
			System.out.println("Price for phone 1: $"+phone1);
			System.out.println("Price for phone 2: $"+phone2);
			System.out.println("Price for phone 3: $"+phone3);
			
	}

}